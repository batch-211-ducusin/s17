console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function getInfo() {
		let fullName = prompt("Enter your Full Name: ");
		let age = prompt("How old are you? ");
		let address = prompt("Where do you live? ");
		alert("Thank you for your inputs!");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " " + "years old.");
		console.log("You live in " + address);
	};
	getInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favoriteBands() {
		let bandOne = "My Chemical Romance";
		let bandTwo = "System Of A Down";
		let bandThree = "Cradle Of Filth";
		let bandFour = "Archenemy";
		let bandFive = "Alice In Chain";

		console.log("1. " + bandOne);
		console.log("2. " + bandTwo);
		console.log("3. " + bandThree);
		console.log("4. " + bandFour);
		console.log("5. " + bandFive);
	};
	favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favoriteMoviesAndRatings() {
		let movieOne = "Snowpiercer";
		let movieTwo = "Now You See Me";
		let movieThree = "Titanic";
		let movieFour = "50 First Dates";
		let movieFive = "The Boy Who Harnessed The Wind";

		let ratingOne = "94%";
		let ratingTwo = "50%";
		let ratingThree = "87%";
		let ratingFour = "45%";
		let ratingFive = "86%"

		console.log("1. " + movieOne);
		console.log("Rotten Tomatoes Rating: " + ratingOne);
		console.log("2. " + movieTwo);
		console.log("Rotten Tomatoes Rating: " + ratingTwo);
		console.log("3. " + movieThree);
		console.log("Rotten Tomatoes Rating: " + ratingThree);
		console.log("4. " + movieFour);
		console.log("Rotten Tomatoes Rating: " + ratingFour);
		console.log("5. " + movieFive);
		console.log("Rotten Tomatoes Rating: " + ratingFive);
	};
	favoriteMoviesAndRatings();




/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/






// printUsers();
// let printFriends() = function printUsers(){
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	// let friend1 = alert("Enter your first friend's name:"); 
	// let friend2 = prom("Enter your second friend's name:");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:");
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	// console.log(friends);
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);