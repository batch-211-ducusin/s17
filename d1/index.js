console.log("Hello World");

//Functions in Javascript are lines or blocks of codes that tell out device or application to perform a certain task when called or invoked. Functions are mostly created to create complicated task to run several lines of code in succession. They are also used to prevent repeating lines or blocks of codes that perform the same task or function


//Function Declarations
	/*
		(Function statement) definces a function with the specified parameters.

		Syntax:
			function functionName() {
				codeblock (statement)
			};

		-Function keyword - used to define a Javascript function.
		-FunctionName - the function name. Functions are named to be able to use later in the code.
		-Function block ({}) - the statements which comprise the body of the function. This is where the code is to be executed.
		-we can assign a variable to hold a function, but that will be explained later.
	*/

	function printName() {
		console.log("My Name is AJ");
	};
	printName();
		//-Semicolons are used to seperate executable javascript statements.



//Function Invocation
	/*
		-The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
		-It is common to use the term "call a function" instead of "invoke a function"
	*/

	printName();

	/*declaredFunction();*/	//error.	much like variable, we cannot invoke a function we have yet to define.



//Function Declaration vs Expressions
	/*
		Function Declaration
		- A function can be create through function declaration by using the function keyword and adding a function name.
		- declared functions are not executed immediately. They are "saved" for later use, and will be executed later when they are invoked (call upon)
	*/

	declaredFunction();	//declared functions can be hoisted, as long as the function has been defined.

	function declaredFunction() {
		console.log("Hello World from declaredFunction")
	};

	/*NOTE:
		Hoisting is JS's behavior for certain variables and functions to run ro use them before their declaration*/



//Function Expression
	/*
		- A function can also be stored in a variable. This is called a function Expression.
		- A function expression is an anonymous function assigned to the variableFunction
		- Anonymous Function - function without a name.
	*/

	/*
		Function Expression, being stored in a let or const variabl cannot be hoisted
	*/
	let variableFunction = function() {
		console.log("Hello Again");
	};
	variableFunction();


	//We can also create a function expression of a named function.
	//However, to invoke the function expression, we invoke it by its variable name, not by its function name.
	//Function Expression are always invoked using the variable name
	let funcExpression = function funcName() {
		console.log("Hello From the Other Side");
	};
	//funcName();	//error
	funcExpression();


	//you can reassign declared cuntions and function expressions to new anonumous functions.
	declaredFunction = function() {
		console.log("updated declaredFunction");
	};
	declaredFunction();


	funcExpression = function() {
		console.log("updated funcExpression");
	};
	funcExpression();


	//However, we cannot re-assign a function expression initialized with const.
	const constantFunc = function() {
		console.log("Initialized with const");
	};
	constantFunc();

	/*constantFunc = function() {
		console.log("Cannot be re-assigned!");
	};
	constantFunc();*/



//Function Scoping
	/*
		Scope is the accessibility (visibility) of variables within our program.
		
		JS Variable has 3 types of scope:
			1. local / global scope
			2. global scope
			3. cintion scope
	*/

	{
		let localVar = "Armando Perez";
		console.log(localVar);
		//console.log(globalVar);	//error
		//pero kapag na declare sya sa taas ng code block, puwede syang tawagin sa loob.
	}
	let globalVar = "Mr. Worldwide";
	console.log(globalVar);
	//console.log(localVar);	//error
	//localVar being in a block, cannot be accessed outside of its  code block.



//Function Scope
	/*
		JS has a function scope: Each function creates a new scope Variable defined inside a function are not accessible (visible) from outside the function.
		Variable declared with var, let, and const are quite similar when declared inside a function.
	*/

		function showName() {
			//Function scoped variables
			var functionVar = "Joe";
			const functionConst = "John";
			let functionLet = "Jane";

			console.log(functionVar);
			console.log(functionConst);
			console.log(functionLet);
		};
			showName();
			// console.log(functionVar);	//error
			// console.log(functionConst);	//error
			// console.log(functionLet);	//error

	/*
		the variables, functionVar, functionConst, and functionLet, are function scoped and cannot be accessed outside of the function of the function they were declared in.
	*/



//Nested Functions
	/*
		- You can create another function inside a function.
		- This is called a nested function.
	*/

	function myNewFunction () {
		let name = "Cee";
		function nestedFunction () {
			let nestedName = "Thor";
			console.log(name);
			console.log(nestedName);
		};
		// console.log(nestedName);	//result to an error. nestedName is not defined. nestedName variable, being declared in the nestedFunction cannot be accessed outside of the function it was declared in.
		nestedFunction();
	};
	myNewFunction();
	// nestedFunction();
	/*
		since this function is declared inside myNewFunction, it too cannot be invoked outside of the function it was declared in.
	*/



//Function and Global Scope Variables
	let globalName = "Nej"; //This is a global scope variable

	function myNewFunction2 () {
		let nameInside = "Martin";
		//Variables declared globally (outside any function) have global scope.
		//global variables can be accessed from anywhere in a JS program including from inside a function.
		console.log(globalName);
	};
	myNewFunction2();



//Using alert()
	/*
		- alert() allows us to show a small window at the top of our browswer page to whow information to our users
		- as opposed to a console.log(); which will only show the message on the console
		- it allows us to show a short dialog or instruction to our user.
		- the page will wait until the user dismisses the dialog
	*/

	alert("Hello World");	//This will run immediately when the page loads

	/*
		alert() syntax
		alert("<messageInString>");
		- you can do it numbers too

		- you can also use an alert() to show a message to the user from a later unction invocation
	*/

	function showSampleAlert() {
		alert("Hello Alert");
	};
	showSampleAlert();

	//you will find that the page waits for the user to dismiss the dialog before proceeding.
	//you can witness this by reloading the page while the console is open.
	console.log("I will only log in the console when the alert is dismissed.");

	//Notes on the use of alert();
		//show only an alert() for short dialog or messages to the user.
		//do not overuse alert() because the program or JS has to wait fot it to be dismissed before continuing.



//Using prompt()
	/*
		- prompt() allows us to show a small window at the top of our browser to gather user input.
		- it, much like alert(), will have the page wait until the user completes or enters their input.
		- the input from the prompt() will be returned as string once the user dismisses the window.
	*/

		let samplePrompt = prompt("Enter your Name: ");
		console.log("Hello, " + samplePrompt);

	/*
		prompt() Syntax:
		prompt("<dialogInString>");
	*/

		let sampleNullPrompt = prompt("Don't enter anything");
		console.log(sampleNullPrompt);
		//prompt() returns an empty string when there is no input.
		//null if the user cancels the prompt()
		//prompt() can be used for us to gather user input and be used in our code.
		//however, since prompt() windows will have the page wait until the user dimisses the window it must not be overused.
		//prompt() used globally will be run immediately, so, for better user experience, it is much better to use them accordingly or add them in a function.

		function printWelcomeMessage() {
			let firstName = prompt("Enter your Fist Name: ");
			let lastName = prompt("Enter your Last Name: ");
			console.log("Hello, " + firstName + " " + lastName + "!");
		};
		printWelcomeMessage();



//Function Naming Conventions
	/*
		- function names should be definitive of the task it will perform. It usually contains a verb.
	*/

	function getCourses() {
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	};
	getCourses();

	//avoid generic names to avoid confusion within your code

	function get() {
		let name = "Jamie";
		console.log(name);
	};
	get();

	//avoid pointless and inappropriate function names

	function pikachu() {
		console.log(25%5);
	};
	pikachu();

	//Name your function in small caps. Follow camelCase when naming variables and function.

	function displayCarInfo() {
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000");
	}
	displayCarInfo();